<?php

require 'GPSTracker.php';

$cfg = require 'config.php';

$debug = isset($cfg['debug']) && $cfg['debug'];

if ($debug) {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
}

session_start();

try {
    $response = (new GPSTracker($cfg))->execute($_REQUEST);
} catch (Exception $e) {
    $response = new Response(
        500,
        $debug ? $e->getMessage() : 'Something went wrong'
    );
}

$response->emit();
