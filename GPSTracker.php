<?php

declare(strict_types = 1);

require_once 'Response.php';
require_once 'Query.php';

class GPSTracker
{
    const ACTIONS = [
        'status' => 'statusAction',
        'auth' => 'authAction',
        'logout' => 'logoutAction',
        'addtrack' => 'addtrackAction',
        'addpos' => 'addposAction',
        'get-tracks' => 'getTracksAction',
        'get-positions' => 'getPositionsAction',
        //'create-user' => 'createUser',
        //'install' => 'install',
    ];

    protected $config;
    protected $request;
    protected $db;

    protected $pdo_options = [
        \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
        \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
        \PDO::ATTR_EMULATE_PREPARES => false,
    ];

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function execute($request)
    {
        $this->request = $request;

        if (!$action = $this->request('action')) {
            return new Response(400, 'Action is missing');
        }

        if (!array_key_exists($action, static::ACTIONS)) {
            return new Response(400, 'Invalid action');
        }

        return $this->{static::ACTIONS[$action]}();
    }

    public function db(): \PDO
    {
        return $this->db ?? $this->db = new \PDO(
            'sqlite:' . $this->cfg()['database'],
            null,
            null,
            $this->pdo_options
        );
    }

    public function cfg()
    {
        return $this->config;
    }

    protected function request($key, $else = null)
    {
        return $this->request[$key] ?? $else;
    }

    protected function install()
    {
        if (file_exists($this->cfg()['database'])) {
            return new Response(400, 'Database already exists');
        }

        if (!file_exists('schema.sql')) {
            return new Response(500, 'Schema file does not exist');
        }

        $schema = file_get_contents('schema.sql');

        return $this->db()->exec($schema)
            ? new Response(200, 'Install successful')
            : new Response(500, 'Installation failed');
    }

    protected function createUser()
    {
        $stmt = $this->db()->prepare('
            INSERT INTO
                users (name, password, added)
            VALUES
                (?, ?, CURRENT_TIMESTAMP)
        ');

        $exec = $stmt->execute([
            $this->request('user'),
            password_hash($this->request('pass'), PASSWORD_DEFAULT),
        ]);

        return $exec
            ? new Response(200, 'User created')
            : new Response(500, 'Failed to create user');
    }

    public function isLogged()
    {
        return !empty($_SESSION['user_id']);
    }

    protected function statusAction()
    {
        return new Response(200, [
            'authed' => $this->isLogged(),
        ]);
    }

    protected function authAction()
    {
        if ($this->request('pass') && $this->request('user')) {
            $stmt = $this->db()->prepare('
                SELECT * FROM
                    users
                WHERE
                    name = ?
                LIMIT
                    1
            ');

            $stmt->execute([
                $this->request('user'),
            ]);

            if (($user = $stmt->fetch())
                && password_verify($this->request('pass'), $user['password'])
            ) {
                $_SESSION['user_id'] = $user['id'];
                return new Response(200, 'Successfully logged in');
            }
        }

        return new Response(401, 'Invalid username or password');
    }

    protected function logoutAction()
    {
        unset($_SESSION['user_id']);

        return new Response(200);
    }

    protected function addtrackAction()
    {
        if (!$this->isLogged()) {
            return new Response(401, 'Unauthorized');
        }

        $stmt = $this->db()->prepare('
            INSERT INTO
                tracks (user_id, name, comment, added)
            VALUES
                (?, ?, ?, CURRENT_TIMESTAMP)
        ');

        $exec = $stmt->execute([
            $_SESSION['user_id'],
            $this->request('track'),
            $this->request('comment'),
        ]);

        return $exec
            ? new Response(200, [
                'message' => 'Track created',
                'trackid' => $this->db()->lastInsertId(),
            ])
            : new Response(500, 'Failed to create track');
    }

    protected function addposAction()
    {
        if (!$this->isLogged()) {
            return new Response(401, 'Unauthorized');
        }

        $stmt = $this->db()->prepare('
            INSERT INTO
                positions (
                    user_id, track_id, time, latitude, longitude, altitude,
                    speed, bearing, accuracy, provider, comment, ip, added
                )
            VALUES
                (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP)
        ');

        $exec = $stmt->execute([
            $_SESSION['user_id'],
            $this->request('trackid'),
            $this->request('time'),
            $this->request('lat'),
            $this->request('lon'),
            $this->request('altitude'),
            $this->request('speed'),
            $this->request('bearing'),
            $this->request('accuracy'),
            $this->request('provider'),
            $this->request('comment'),
            $_SERVER['REMOTE_ADDR'],
        ]);

        return $exec
            ? new Response(200, 'Position saved')
            : new Response(500, 'Failed to save position');
    }

    protected function getTracksAction()
    {
        if (!$this->isLogged()) {
            return new Response(401, 'Unauthorized');
        }

        $count = $this->count('tracks');

        $stmt = $this->db()->prepare('
            SELECT
                id, name, comment, added
            FROM
                tracks
            ORDER BY
                added ASC
            LIMIT
                ?
            OFFSET
                ?
        ');

        $limit = (int) $this->request('limit', 1000);
        $page = (int) $this->request('page', 1);
        $page -= 1;

        $stmt->execute([
            $limit,
            $page * $limit
        ]);

        return new Response(200, [
            'rows' => $stmt->fetchAll(),
            'pages' => ceil($count / $limit),
        ]);
    }

    // TODO: improve this
    protected function getPositionsAction()
    {
        if (!$this->isLogged()) {
            return new Response(401, 'Unauthorized');
        }

        $limit = (int) $this->request('limit', 10000);
        $page = $this->request('page', 1) - 1;

        $count_query = Query::select()
            ->columns('COUNT(id)')
            ->table('positions');

        $query = Query::select()
            ->columns('latitude', 'longitude')
            ->table('positions')
            ->order('positions.added DESC')
            ->limit($limit);

        if ($start = $this->request('start', false)) {
            $count_query->where('added >= ?', $start);
            $query->where('added >= ?', $start);
        }

        if ($end = $this->request('end', false)) {
            $count_query->where('added <= ?', $end);
            $query->where('added <= ?', $end);
        }

        $count_stmt = $this->db()->prepare($count_query->sql());
        $count_stmt->execute($count_query->values());

        $count = (int) $count_stmt->fetchColumn(0);

        $query->offset($page * $limit);

        $stmt = $this->db()->prepare($query->sql());

        $stmt->execute($query->values());

        return new Response(200, [
            'rows' => $stmt->fetchAll(),
            'pages' => ceil($count / $limit),
        ]);
    }

    protected function count($table)
    {
        return (int) $this->db()->query(
            sprintf(
                'SELECT COUNT(id) FROM %s',
                $table
            )
        )->fetchColumn(0);
    }
}
