<?php

declare(strict_types = 1);

class Response
{
    const HTTP_STATUS = [
        200 => 'OK',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        403 => 'Forbidden',
        500 => 'Internal Server Error',
    ];

    protected $code;
    protected $message;
    protected $headers = [
        'Content-Type' => 'application/json',
    ];

    public function __construct(
        int $code,
        $message = null,
        array $headers = []
    ) {
        $this->code = $code;
        $this->message = $message;
        $headers && $this->headers = array_merge($this->headers, $headers);
    }

    public function emit()
    {
        header(sprintf(
            '%s %d %s',
            $_SERVER['SERVER_PROTOCOL'],
            $this->code,
            static::HTTP_STATUS[$this->code]
        ));

        foreach ($this->headers as $k => $v) {
            header("$k: $v");
        }

        $output = [
            'error' => $this->code < 200 || $this->code >= 300
        ];

        if (is_array($this->message)) {
            $output += $this->message;
        } else {
            $output['message'] = $this->message;
        }

        echo json_encode($output);
    }
}
