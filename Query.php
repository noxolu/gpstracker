<?php

declare(strict_types = 1);

class Query
{
    protected $expression;
    protected $distinct;
    protected $table;
    protected $columns = [];
    protected $joins = [];
    protected $wheres;
    protected $group;
    protected $having;
    protected $order;
    protected $limit;
    protected $offset;

    protected $values = [
        'where' => [],
        'limit' => [],
        'offset' => [],
    ];

    public static function select(): self
    {
        return new static('SELECT');
    }

    protected function __construct(string $expression)
    {
        $this->expression = $expression;
    }

    public function distinct(bool $state = true): self
    {
        $this->distinct = $state;

        return $this;
    }

    public function table(string $table): self
    {
        $this->table = $table;

        return $this;
    }

    public function columns(string ...$columns): self
    {
        $this->columns = $columns;

        return $this;
    }

    public function join(
        string $table,
        string $on,
        string $type = 'INNER'
    ): self {
        $this->joins[] = sprintf(
            '%s %s ON %s',
            ltrim($type . ' JOIN'),
            $table,
            $on
        );

        return $this;
    }

    public function leftJoin(string $table, string $on): self
    {
        return $this->join($table, $on, 'LEFT');
    }

    public function where($criteria, ...$values): self
    {
        $this->wheres[] = $criteria;
        array_push($this->values['where'], ...$values);

        return $this;
    }

    public function group(...$columns): self
    {
        $this->group = $columns;

        return $this;
    }

    public function having($criteria): self
    {
        $this->having = $criteria;

        return $this;
    }

    public function order(...$columns): self
    {
        $this->order = $columns;

        return $this;
    }

    public function limit($value): self
    {
        $this->limit = '?';
        $this->values['limit'] = $value;

        return $this;
    }

    public function offset($value): self
    {
        $this->offset = '?';
        $this->values['offset'] = $value;

        return $this;
    }

    public function assembleSelect()
    {
        $sql = $this->expression;

        if ($this->distinct) {
            $sql .= ' DISTINCT';
        }

        $sql .= "\n";

        $sql .= '    ';
        $sql .= implode(",\n    ", $this->columns);

        $sql .= "\nFROM\n    " . $this->table . "\n";

        if ($this->joins) {
            $sql .= implode("\n", $this->joins);
            $sql .= "\n";
        }

        if ($this->wheres) {
            $sql .= "WHERE\n    ";
            $sql .= implode("\n    AND ", $this->wheres);
            $sql .= "\n";
        }

        if ($this->group) {
            $sql .= "GROUP BY\n    ";
            $sql .= implode(",\n    ", $this->group);
            $sql .= "\n";
        }

        if ($this->having) {
            $sql .= "HAVING\n    " . $this->having . "\n";
        }

        if ($this->order) {
            $sql .= "ORDER BY\n    ";
            $sql .= implode(",\n    ", $this->order);
            $sql .= "\n";
        }

        if ($this->limit) {
            $sql .= "LIMIT\n    " . $this->limit . "\n";
        }

        if ($this->offset) {
            $sql .= "OFFSET\n    " . $this->offset . "\n";
        }

        return $sql;
    }

    public function values()
    {
        $values = [];

        foreach ($this->values as $value) {
            if (!is_array($value)) {
                $values[] = $value;
            } elseif (count($value)) {
                array_push($values, ...$value);
            }
        }

        return $values;
    }

    public function sql()
    {
        return $this->assembleSelect();
    }
}
