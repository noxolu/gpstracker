CREATE TABLE `users` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	TEXT NOT NULL UNIQUE,
	`password`	TEXT NOT NULL,
	`added`	TEXT
);
CREATE TABLE `tracks` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`user_id`	INTEGER NOT NULL,
	`name`	TEXT,
	`comment`	INTEGER,
	`added`	INTEGER
);
CREATE TABLE IF NOT EXISTS "positions" (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`user_id`	INTEGER NOT NULL,
	`track_id`	INTEGER NOT NULL,
	`time`	INTEGER NOT NULL,
	`latitude`	REAL NOT NULL,
	`longitude`	REAL NOT NULL,
	`altitude`	REAL,
	`speed`	REAL,
	`bearing`	REAL,
	`accuracy`	INTEGER,
	`provider`	TEXT,
	`comment`	TEXT,
	`ip`	TEXT,
	`added`	TEXT
);
